﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Web.Http;
using System.Net.Http;
using System.Net;

namespace BoxAgileDev
{
    public class HttpResponseManager<T> : ApiController
    {
        public HttpResponseManager()
        {
        }

        /// <summary>
        /// Class Business Management (BM) to process control.
        /// </summary>
        private T _instance;
        private PostUtil _postUtility;

        /// <summary>
        /// Instance of Business Logic Controller for this service. (BM)
        /// </summary>
        public T Instance
        {
            get
            {
                bool isEmpty = EqualityComparer<T>.Default.Equals(_instance, default(T));
                _instance = isEmpty == true ? Activator.CreateInstance<T>() : _instance;
                return _instance;
            }
            set
            {
                _instance = value;
            }
        }

        /// <summary>
        /// Function for convert data in format Json to other Type. 
        /// </summary>
        public PostUtil PostUtility
        {
            get
            {
                _postUtility = _postUtility ?? new PostUtil();
                return _postUtility;
            }
        }

        /// <summary>
        /// Method for build a Http response with the flow control state.
        /// </summary>
        /// <param name="result">Result of process</param>
        /// <param name="request">Request of client</param>
        /// <returns></returns>
        public HttpResponseMessage HandleResult(Result result, HttpRequestMessage request)
        {
            switch (result.FlowControl)
            {
                case FlowOptions.Success: return request.CreateResponse(HttpStatusCode.OK, result);
                case FlowOptions.Error: return request.CreateResponse(HttpStatusCode.InternalServerError, result);
                case FlowOptions.Stop: return request.CreateResponse(HttpStatusCode.Conflict, result);
                case FlowOptions.Nothing: return request.CreateResponse(HttpStatusCode.NoContent, result);
                case FlowOptions.Started: return request.CreateResponse(HttpStatusCode.Forbidden, result);
                default: return request.CreateResponse(HttpStatusCode.Accepted, result);
            }
        }

    }

    /// <summary>
    /// Class for convert data in JSon to other Type.
    /// </summary>
    public class PostUtil
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T">Generic type to return</typeparam>
        /// <param name="data">Object y Json</param>
        /// <returns></returns>
        public T Get<T>(dynamic data)
        {
            if (data != null)
            {
                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.NullValueHandling = NullValueHandling.Ignore;
                return JsonConvert.DeserializeObject<T>(data.ToString(), settings);
            }
            return default(T);
        }
    }

    
}
