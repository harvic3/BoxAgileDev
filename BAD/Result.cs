﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;

namespace BoxAgileDev
{
    /// <summary>
    /// Class for manage flow process control and return data and messages to client.
    /// </summary>
    public class Result
    {
        /// <summary>
        /// Variable for storage Data that will be returned to the client.
        /// </summary>
        private dynamic _data;

        /// <summary>
        /// Variable of list messages that will be returned to the client.
        /// </summary>
        private List<string> _messages;

        /// <summary>
        /// Variable for to assign to Flow control process in a call to a function.
        /// </summary>
        private FlowOptions _flowControl;

        /// <summary>
        /// Variable mirror of the flow control with text value for the client.
        /// </summary>
        private string _flowState;

        /// <summary>
        /// Flag for to assign to Flow control process in a call to a function.
        /// </summary>
        public FlowOptions FlowControl { 
            get { return _flowControl; }
            set
            {
                _flowControl = value;
                switch (_flowControl)
                {
                    case FlowOptions.Error:
                        _flowState = "Error";
                        break;
                    case FlowOptions.Success:
                        _flowState = "Success";
                        break;
                    case FlowOptions.Stop:
                        _flowState = "Stopped";
                        break;
                    case FlowOptions.Started:
                        _flowState = "Started";
                        break;
                    case FlowOptions.Nothing:
                        _flowState = "Nothing";
                        break;
                }
            }
        }

        /// <summary>
        /// Variable for access easy of the state of FlowControl for the client.
        /// </summary>
        public string FlowState { get { return _flowState; } }

        /// <summary>
        /// Simple error or success message for return to client.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Multiple error or success message for return to client.
        /// </summary>
        public List<string> Messages
        {
            get
            {
                _messages = _messages ?? new List<string>();
                return _messages;
            }
            set { _messages = value; }
        }
        
        /// <summary>
        /// Variable to storage the data that will be returned to the client.
        /// </summary>
        public dynamic Data {
            get { return _data; }
            set { _data = value; }
        }
        
        /// <summary>
        /// Flag for quick access to Error and Stop Flow Control. 
        /// Return True if Error and Stop flag in the Flow Control are false. 
        /// </summary>
        public bool Continue
        {
            get
            {
                return (_flowControl != FlowOptions.Error && _flowControl != FlowOptions.Stop) ? true : false;
            }
        }

        /// <summary>
        /// Class Constructor.
        /// </summary>
        public Result()
        {
            _flowControl = FlowOptions.Started;
            _flowState = "Started";
        }

        /// <summary>
        /// Add a Message to ListMessage.
        /// </summary>
        /// <param name="message">Message to add to ListMessages</param>
        public void AddMessageToList(string message)
        {
            _messages = _messages ?? new List<string>();
            _messages.Add(message);
        }

        /// <summary>
        /// Delete a last Message of ListMessage.
        /// </summary>
        public void DeleteLastMessageList()
        {
            if (_messages != null && _messages.Count > 0)
            {
                _messages.RemoveAt(_messages.Count - 1);
            }
        }

        /// <summary>
        /// Get List Messages in a unique String joined by separator.
        /// </summary>
        /// <param name="separator">Separator to join the List Messages.</param>
        /// <returns></returns>
        public string GetMessages(string separator)
        {
            if (_messages != null && _messages.Count == 0)
            {
                return Message;
            }
            return String.Join(separator, _messages);
        }

        /// <summary>
        /// Function for storage multiple data object in Data variable (one to one) that will be returned to the client.
        /// </summary>
        /// <typeparam name="T">Type object to be storage.</typeparam>
        /// <param name="name">Name object to be storage.</param>
        /// <param name="value">Object to storage in Data Result.</param>
        public void AddData<T>(string name, T value)
        {
            _data = _data ?? new ExpandoObject();
            var data = _data as IDictionary<String, object>;
            data[name] = value;
        }

    }
    
    /// <summary>
    /// Options to assign to FlowControl process.
    /// </summary>
    public enum FlowOptions
    {        
        /// <summary>
        /// The process have Error or Errors.
        /// </summary>
        [Description("Error(s) in process.")]
        Error = 2,
        /// <summary>
        /// The process is completed. 
        /// </summary>
        [Description("Successful process.")]
        Success = 1,
        /// <summary>
        /// The process must Stop.
        /// </summary>
        [Description("Process stopped.")]
        Stop = 3,
        /// <summary>
        /// Nothing was done in the process.
        /// </summary>
        [Description("Any change in process.")]
        Nothing = 4,
        /// <summary>
        /// The process started. (Default by Constructor)
        /// </summary>
        [Description("Process initiated.")]
        Started = 5
    }

}
