﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace BoxAgileDev
{
    /// <summary>
    /// Class for mapping and emulate objects.
    /// </summary>
    public static class SimpleMapper
    {
        /// <summary>
        /// Function to map a object to other similar object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="origin">Object of origin data.</param>
        /// <param name="dest">Object to destination data.</param>
        /// <returns></returns>
        public static T Map<T>(object origin, T dest)
        {
            if (origin == null)
            {
                return default(T);
            }

            var properties = origin.GetType();

            var originProperties = dest.GetType().GetProperties();

            foreach (var prop in originProperties)
            {
                PropertyInfo value = properties.GetProperty(prop.Name);

                if (value != null)
                {
                    prop.SetValue(dest, value.GetValue(origin, null), null);
                }
            }
            return dest;
        }

        /// <summary>
        /// FUnction to map the properties a type object to other similar.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="origin"></param>
        /// <returns></returns>
        public static T Map<T>(object origin)
        {
            T dest = Activator.CreateInstance<T>();
            return Map<T>(origin, dest);
        }
        
        /// <summary>
        /// Function for map a collection of objects.
        /// </summary>
        /// <typeparam name="T">The type of object to be converted.</typeparam>
        /// <param name="Data">List of object to map.</param>
        /// <returns></returns>
        public static List<T> MapCollection<T>(dynamic Data)
        {
            List<T> results = new List<T>();
            foreach (var item in Data)
            {
                results.Add(Map<T>(item));
            }
            return results;
        }

        /// <summary>
        /// Function for emulate a List of a type object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="v"></param>
        /// <returns></returns>
        public static List<T> Emulate<T>(int v)
        {
            List<T> lista = new List<T>();
            for (int i = 0; i < v; i++)
            {
                T newItem = Activator.CreateInstance<T>();
                lista.Add(newItem);
            }
            return lista;
        }
    }
}


