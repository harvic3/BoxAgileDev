# Box Agile Dev (BAD)

> BoxAgileDev is a set of tools that will support you to perform some repetitive tasks that are executed in the development of any software project in which you interact with object models, business rules and data access of those models.

The tools in this package are:

- Result: is a tool for Flow Process Control and return data of your application.

- SimpleMapper: is a simple tool for mapping object models with data models.

- HttpResponseManager: is a tool for management the Api Controllers and Map Json objects in Object models (PostUtility) into your application.

- PostUtility is a Class into HttpResponseManager with which you can map objects in Json format to object models.

## Installation
Go to management Nuget Package in your project and searh `BAD` and install `Vickodev.Utility.BAD`.

Ok, once installed, go to any Class in the project and declare the library for use it, `using BoxAgileDev;` and ready.